{{--@servers(['web' => 'test.com'])--}}

{{--@setup--}}
{{--$repository = 'git@gitlab.com:mrkrishnsharma/laravelcrud.git';--}}
{{--$releases_dir = '/var/www/html/releases';--}}
{{--$app_dir = '/var/www/html';--}}
{{--$release = date('YmdHis');--}}
{{--$new_release_dir = $releases_dir .'/'. $release;--}}
{{--@endsetup--}}

{{--@story('deploy')--}}
{{--clone_repository--}}
{{--run_composer--}}
{{--update_symlinks--}}
{{--run_migrations--}}
{{--@endstory--}}

{{--@task('clone_repository')--}}
{{--echo 'Cloning repository'--}}
{{--[ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}--}}
{{--git clone --depth 1 {{ $repository }} {{ $new_release_dir }}--}}
{{--@endtask--}}


{{--@task('run_composer')--}}
{{--echo "Starting deployment ({{ $release }})"--}}
{{--cd {{ $new_release_dir }}--}}
{{--composer install --prefer-dist --no-scripts -q -o--}}
{{--cp .env.example .env--}}
{{--@endtask--}}



{{--@task('update_symlinks')--}}
{{--echo "Linking storage directory"--}}
{{--rm -rf {{ $new_release_dir }}/storage--}}
{{--ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage--}}

{{--echo 'Linking .env file'--}}
{{--ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env--}}

{{--echo 'Linking current release'--}}
{{--ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current--}}
{{--@endtask--}}

{{--@task('run_migrations')--}}
{{--echo "Starting Migrations ({{ $release }})"--}}
{{--cd {{ $new_release_dir }}--}}
{{--php artisan migrate --force--}}
{{--php artisan db:seed--}}
{{--@endtask--}}
