FROM php:7.2-fpm-alpine

RUN docker-php-ext-install pdo pdo_mysql

# Install Composer
RUN curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install Laravel Envoy
RUN composer global require "laravel/envoy=~1.0"