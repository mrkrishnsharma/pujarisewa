<?php

namespace Tests\Unit;

use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Tests\TestCase;
use App\User;
use Faker\Factory as Faker;
use Illuminate\Http\Request;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserControllerTest extends TestCase
{
    use DatabaseTransactions;

    private  $testData;
    private $faker;


    public function setUp(): void
    {
        parent::setUp();

        $this->faker = Faker::create();

        $this->testData = [
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'email' => $this->faker->unique()->email,
            'mobile' => rand(999999999,9999999999),
            'dob' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'address' => $this->faker->address,
        ];
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    public function testExample()
    {
        $this->assertTrue(true);
    }


    /** @test */
    public function can_create_a_user()
    {

        $UserValidation = new StoreRequest();

        $validator = Validator::make($this->testData, $UserValidation->rules());

        $this->assertTrue($validator->passes());

        $response = $this->json('POST','api/users',$this->testData);

        $response->assertStatus(201);

        \Log::info($response->getContent());

        $response->assertJsonStructure([
            'data' =>
                 ['id', 'first_name', 'last_name', 'email', 'mobile','dob','address']]);

        $this->assertDatabaseHas('users', [
            'first_name' => $this->testData['first_name'],
            'last_name' => $this->testData['last_name'],
            'email' => $this->testData['email'],
            'mobile' => $this->testData['mobile'],
            'dob' => $this->testData['dob'],
            'address' => $this->testData['address'],
        ]);

    }

    /** @test */
    public function can_update_a_user()
    {
        $user = factory(User::class)->create();


        $UserValidation = new UpdateRequest();

        $validator = Validator::make($this->testData, $UserValidation->rules());

        $this->assertTrue($validator->passes());

        $response = $this->json('PATCH','api/users/'.$user->id,$this->testData);

        $response->assertStatus(200);

        \Log::info($response->getContent());

        $response->assertJsonStructure([
            'data' =>
                ['id', 'first_name', 'last_name', 'email', 'mobile','dob','address']]);

        $this->assertDatabaseHas('users', [
            'first_name' => $this->testData['first_name'],
            'last_name' => $this->testData['last_name'],
            'email' => $this->testData['email'],
            'mobile' => $this->testData['mobile'],
            'dob' => $this->testData['dob'],
            'address' => $this->testData['address'],
        ]);

    }

    public function test_can_return_A_single_user(){

        $user = factory(User::class)->create();

        $response = $this->json('GET','api/users/'.$user->id);

        \Log::info($response->getContent());

        $response->assertJsonStructure([
            'data' =>
                ['id', 'first_name', 'last_name', 'email', 'mobile','dob','address']]);

        $response->assertStatus(200)
            ->assertExactJson([
                'data' =>[
                'id' => $user->id,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'email' => $user->email,
                'mobile' => (string)$user->mobile,
                'dob' => $user->dob,
                'address' => $user->address,
            ]]);
    }

    public function test_returns_users_list_with_paginated_Datas()
    {

        $user1 = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        $user3 = factory(User::class)->create();

        $response = $this->json('GET','api/users')
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => ['first_name', 'last_name', 'email', 'mobile','dob','address']
                ],
                'links' => ['first', 'last', 'prev', 'next'],
                'meta' => [
                    'current_page', 'last_page', 'from', 'to',
                    'path', 'per_page', 'total'
                ]
            ]);
    }

    public function test_will_fail_with_a_404_if_user_notfound()
    {
        $this->json('DELETE', 'api/users/' . -1)
            ->assertStatus(404)
            ->assertSee(null);

    }

    public function test_can_delete_user()
    {
        $user = factory(User::class)->create();

        $this->json('DELETE', 'api/users/' . $user->id)
            ->assertStatus(204)
            ->assertSee(null);

        $this->assertSoftDeleted('users', ['id' => $user->id]);
    }


//    validation tests

    public function test_request_should_fail_when_no_first_name_is_provided()
    {
        $this->testData['first_name'] = null;
        $response = $this->json('POST','api/users', $this->testData);
        $response->assertStatus(
            Response::HTTP_UNPROCESSABLE_ENTITY
        );
        $response->assertJsonValidationErrors('first_name');
    }

    public function test_request_should_fail_when_no_last_name_is_provided()
    {
        $this->testData['last_name'] = null;

        $response = $this->json('POST','api/users', $this->testData);

        $response->assertStatus(
            Response::HTTP_UNPROCESSABLE_ENTITY
        );
        $response->assertJsonValidationErrors('last_name');
    }

    public function test_request_should_fail_when_no_email_is_provided()
    {
        $this->testData['email'] = null;
        $response = $this->json('POST','api/users', $this->testData);

        $response->assertStatus(
            Response::HTTP_UNPROCESSABLE_ENTITY
        );
        $response->assertJsonValidationErrors('email');
    }

    public function test_request_should_fail_when_no_mobile_is_provided()
    {
        $this->testData['mobile'] = null;
        $response = $this->json('POST','api/users', $this->testData);
        $response->assertStatus(
            Response::HTTP_UNPROCESSABLE_ENTITY
        );
        $response->assertJsonValidationErrors('mobile');
    }

    public function test_request_should_fail_when_no_address_is_provided()
    {
        $this->testData['address'] = null;
        $response = $this->json('POST','api/users', $this->testData);
        $response->assertStatus(
            Response::HTTP_UNPROCESSABLE_ENTITY
        );
        $response->assertJsonValidationErrors('address');
    }

    public function test_request_should_fail_when_no_email_format_is_incorrect()
    {
        $this->testData['email'] = 'asddddddasdasasdsddsd';
        $response = $this->json('POST','api/users', $this->testData);
        $response->assertStatus(
            Response::HTTP_UNPROCESSABLE_ENTITY
        );
        $response->assertJsonValidationErrors('email');
    }

    public function test_request_should_fail_when_no_mobile_is_alphabetic()
    {
        $this->testData['mobile'] = 'asddddddasdasasdsddsd';
        $response = $this->json('POST','api/users', $this->testData);
        $response->assertStatus(
            Response::HTTP_UNPROCESSABLE_ENTITY
        );
        $response->assertJsonValidationErrors('mobile');
    }

    public function test_request_should_fail_when_no_first_name_is_provided_update()
    {
        $user = factory(User::class)->create();

        $this->testData['first_name'] = null;
        $response = $this->json('PATCH','api/users/'.$user->id, $this->testData);
        $response->assertStatus(
            Response::HTTP_UNPROCESSABLE_ENTITY
        );
        $response->assertJsonValidationErrors('first_name');
    }
}
