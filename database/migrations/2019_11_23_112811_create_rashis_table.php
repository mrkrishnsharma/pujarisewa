<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRashisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rashis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_en');
            $table->string('name_np');
            $table->string('description_en');
            $table->string('description_np');
            $table->text('other');
            $table->tinyInteger('r_order');
            $table->string('image')->nullable();
            $table->text('daily_en');
            $table->text('daily_np');
            $table->text('monthly_en');
            $table->text('monthly_np');
            $table->text('weekly_en');
            $table->text('weekly_np');
            $table->text('yearly_en');
            $table->text('yearly_np');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rashis');
    }
}
