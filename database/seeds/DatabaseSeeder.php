<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(SpecialitiesTableSeeder::class);
         $this->call(RashiTableSeeder::class);
         $this->call(UserSpecialityTableSeeder::class);
         $this->call(DistrictsTableSeeder::class);
    }
}
