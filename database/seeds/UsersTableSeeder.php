<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')
            ->updateOrInsert(
                ['email' => 'mrkrishnsharma@gmail.com'],
                [
                    'email' => 'mrkrishnsharma@gmail.com',
                    'mobile'=>9849463490,
                    'first_name' => 'krishna',
                    'last_name' => 'sharma',
                    'description' => ' description',
                    'address' => ' description',
                    'type' => 'admin',
                    'password' => bcrypt('admin@123'),
                ]
            );
        DB::table('users')
            ->updateOrInsert(
                ['email' => 'subashbasnet0@gmail.com'],
                [
                    'email' => 'subashbasnet0@gmail.com',
                    'mobile'=>9841903209,
                    'first_name' => 'subash',
                    'last_name' => 'basnet',
                    'description' => ' subash description',
                    'address' => ' subash description',
                    'type' => 'admin',
                    'password' => bcrypt('admin@123'),
                ]
            );
        DB::table('users')
            ->updateOrInsert(
                ['email' => 'ibarneupane@gmail.com'],
                [
                    'email' => 'ibarneupane@gmail.com',
                    'mobile'=>9841903210,
                    'first_name' => 'rabi',
                    'last_name' => 'neupane',
                    'description' => ' rabi description',
                    'address' => ' rabi address',
                    'type' => 'admin',
                    'password' => bcrypt('admin@123'),
                ]
            );
        $i = 0;
//        while($i < 20){
//            DB::table('users')
//                ->updateOrInsert(
//                    ['email' => 'pujari'.$i.'@yopmail.com'],
//                    [
//                        'email' =>  'pujari'.$i.'@yopmail.com',
//                        'mobile'=>9849463490 + $i,
//                        'first_name' => 'pujari'.$i,
//                        'last_name' => 'sirname'.$i,
//                        'description' => 'pujari'.$i.'  description',
//                        'address' => 'pujari'.$i.' address',
//                        'cost_day' => rand(500,300),
//                        'experience_year' => rand(1,10),
//                        'type' => 'pujari',
//                        'password' => bcrypt('password'),
//                        'district_id' => \App\Models\District::inRandomOrder()->pluck('id')->first()
//                    ]
//                );
//            $i++;
//        }
    }
}
