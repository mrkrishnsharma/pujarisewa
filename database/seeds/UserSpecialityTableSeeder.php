<?php

use Illuminate\Database\Seeder;

class UserSpecialityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $pujaris = \App\User::where('type','pujari')->get();

        foreach ($pujaris as $pujari){
            $specialities = \App\Models\Speciality::inRandomOrder()->limit(6)->pluck('id');
            $pujari->specialities()->sync($specialities);
        }
    }
}
