<?php

use Illuminate\Database\Seeder;

class DistrictsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('districts')
        ->updateOrInsert(
            ['name_en' => 'Kathmandu'],
            [
                'name_en' => 'Kathmandu',
                'name_np' => 'Kathmandu',
                'status' => '1',
            ]
        );

        DB::table('districts')
        ->updateOrInsert(
            ['name_en' => 'Bhaktapur'],
            [
                'name_en' => 'Bhaktapur',
                'name_np' => 'Bhaktapur',
                'status' => '1',
            ]
        );
        DB::table('districts')
        ->updateOrInsert(
            ['name_en' => 'Lalitpur'],
            [
                'name_en' => 'Lalitpur',
                'name_np' => 'Lalitpur',
                'status' => '1',
            ]
        );
        DB::table('districts')
        ->updateOrInsert(
            ['name_en' => 'Nuwakot'],
            [
                'name_en' => 'Nuwakot',
                'name_np' => 'Nuwakot',
                'status' => '1',
            ]
        );
        DB::table('districts')
            ->updateOrInsert(
                ['name_en' => 'Bharatpur'],
                [
                    'name_en' => 'Bharatpur',
                    'name_np' => 'Bharatpur',
                    'status' => '1',
                ]
            );
        DB::table('districts')
            ->updateOrInsert(
                ['name_en' => 'Dolakha'],
                [
                    'name_en' => 'Dolakha',
                    'name_np' => 'Dolakha',
                    'status' => '1',
                ]
            );
    }
}
