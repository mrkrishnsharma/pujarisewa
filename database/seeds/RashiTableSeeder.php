<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RashiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* mesh */
        DB::table('rashis')
            ->updateOrInsert(
                ['name_en' => 'ARIES'],
                [
                    'name_en' => 'ARIES',
                    'name_np' => 'मेष',
                    'description_np' => '( चु, चे, चो, ला, लि, लु, ले, लो, अ )',
                    'description_en' => 'March 21 - April 19',
                    'image' => '2-aries.png',
                    'other' => json_encode([
                        'lucky_number_en' => '5,6,7',
                        'lucky_color_en' => 'Red, Blue',
                        'lucky_number_np' => '1,2,3',
                        'lucky_color_np' => 'Red, Blue',
                    ]),
                    'daily_en' => '$this->daily_en',
                    'daily_np' => '$this->daily_np',
                    'monthly_en' => '$this->monthly_en',
                    'monthly_np' => '$this->monthly_np',
                    'weekly_en' => '$this->weekly_en',
                    'weekly_np' => '$this->weekly_np',
                    'yearly_en' => '$this->yearly_en',
                    'yearly_np' => '$this->yearly_np',
                    'r_order' => 1
                ]
            );
        /* end here */
        /* brish */
        DB::table('rashis')
            ->updateOrInsert(
                ['name_en' => 'TAURUS'],
                [
                    'name_en' => 'TAURUS',
                    'name_np' => 'बृष',
                    'description_np' => '( इ, उ, ए, ओ, बा, बि, बु, बे, बो )',
                    'description_en' => 'April 20 - May 20',
                    'image' => '7-taurus.png',
                    'other' => json_encode([
                        'lucky_number_en' => '2,3,1',
                        'lucky_color_en' => 'Red, Blue',
                        'lucky_number_np' => '1,2,3',
                        'lucky_color_np' => 'Red, Blue',
                    ]),
                    'daily_en' => '$this->daily_en',
                    'daily_np' => '$this->daily_np',
                    'monthly_en' => '$this->monthly_en',
                    'monthly_np' => '$this->monthly_np',
                    'weekly_en' => '$this->weekly_en',
                    'weekly_np' => '$this->weekly_np',
                    'yearly_en' => '$this->yearly_en',
                    'yearly_np' => '$this->yearly_np',
                    'r_order' => 2
                ]
            );
        /* end here */
        DB::table('rashis')
            ->updateOrInsert(
                ['name_en' => 'GEMINI'],
                [
                    'name_en' => 'GEMINI',
                    'name_np' => 'मिथुन',
                    'description_np' => '( का, कि, कु, घ, ङ, छ, के, को, हा )',
                    'description_en' => 'May 21 - June 20',
                    'image' => '6-gemini.png',
                    'other' => json_encode([
                        'lucky_number_en' => '6,5,1',
                        'lucky_color_en' => 'Red, Blue',
                        'lucky_number_np' => '1,2,3',
                        'lucky_color_np' => 'Red, Blue',
                    ]),
                    'daily_en' => '$this->daily_en',
                    'daily_np' => '$this->daily_np',
                    'monthly_en' => '$this->monthly_en',
                    'monthly_np' => '$this->monthly_np',
                    'weekly_en' => '$this->weekly_en',
                    'weekly_np' => '$this->weekly_np',
                    'yearly_en' => '$this->yearly_en',
                    'yearly_np' => '$this->yearly_np',
                    'r_order' => 3
                ]
            );
        /* karkat */
        DB::table('rashis')
            ->updateOrInsert(
                ['name_en' => 'CANCER'],
                [
                    'name_en' => 'CANCER',
                    'name_np' => 'कर्कट',
                    'description_np' => '(हि, हु, हे, हो, डा, डि, डु, डे, डो )',
                    'description_en' => 'June 21 - July 22',
                    'image' => '12-cancer.png',
                    'other' => json_encode([
                        'lucky_number_en' => '7,2,5',
                        'lucky_color_en' => 'Red, Blue',
                        'lucky_number_np' => '1,2,3',
                        'lucky_color_np' => 'Red, Blue',
                    ]),
                    'daily_en' => '$this->daily_en',
                    'daily_np' => '$this->daily_np',
                    'monthly_en' => '$this->monthly_en',
                    'monthly_np' => '$this->monthly_np',
                    'weekly_en' => '$this->weekly_en',
                    'weekly_np' => '$this->weekly_np',
                    'yearly_en' => '$this->yearly_en',
                    'yearly_np' => '$this->yearly_np',
                    'r_order' => 4
                ]
            );
        /* end here */
        /* singha */
        DB::table('rashis')
            ->updateOrInsert(
                ['name_en' => 'LEO'],
                [
                    'name_en' => 'LEO',
                    'name_np' => 'सिंह',
                    'description_np' => '( मा, मि, मु, मे, मो, टा, टि, टु, टे )',
                    'description_en' => 'July 23 - August 22',
                    'image' => '4-leo.png',
                    'other' => json_encode([
                        'lucky_number_en' => '6,3,7',
                        'lucky_color_en' => 'Red, Blue',
                        'lucky_number_np' => '1,2,3',
                        'lucky_color_np' => 'Red, Blue',
                    ]),
                    'daily_en' => '$this->daily_en',
                    'daily_np' => '$this->daily_np',
                    'monthly_en' => '$this->monthly_en',
                    'monthly_np' => '$this->monthly_np',
                    'weekly_en' => '$this->weekly_en',
                    'weekly_np' => '$this->weekly_np',
                    'yearly_en' => '$this->yearly_en',
                    'yearly_np' => '$this->yearly_np',
                    'r_order' => 5
                ]
            );
        /* end here */
        /* kanya */
        DB::table('rashis')
            ->updateOrInsert(
                ['name_en' => 'VIRGO'],
                [
                    'name_en' => 'VIRGO',
                    'name_np' => 'कन्या',
                    'description_np' => '( टो, प, पि, पु, ष, ण, ठ, पे, पो )',
                    'description_en' => 'August 23 - September 22.',
                    'image' => '10-virgo.png',
                    'other' => json_encode([
                        'lucky_number_en' => '8,9,1',
                        'lucky_color_en' => 'Red, Blue',
                        'lucky_number_np' => '8,9,1',
                        'lucky_color_np' => 'Red, Blue',
                    ]),
                    'daily_en' => '$this->daily_en',
                    'daily_np' => '$this->daily_np',
                    'monthly_en' => '$this->monthly_en',
                    'monthly_np' => '$this->monthly_np',
                    'weekly_en' => '$this->weekly_en',
                    'weekly_np' => '$this->weekly_np',
                    'yearly_en' => '$this->yearly_en',
                    'yearly_np' => '$this->yearly_np',
                    'r_order' => 6
                ]
            );
        /*end here */
        /* tula */
        DB::table('rashis')
            ->updateOrInsert(
                ['name_en' => 'LIBRA'],
                [
                    'name_en' => 'LIBRA',
                    'name_np' => 'तुला',
                    'description_np' => '(र, रि, रु, रे, रो, ता, ति, तु, ते)',
                    'description_en' => 'September 23 - October 22',
                    'image' => '5-libra.png',
                    'other' => json_encode([
                        'lucky_number_en' => '1,2,3',
                        'lucky_color_en' => 'Red, Blue',
                        'lucky_number_np' => '1,2,3',
                        'lucky_color_np' => 'Red, Blue',
                    ]),
                    'daily_en' => '$this->daily_en',
                    'daily_np' => '$this->daily_np',
                    'monthly_en' => '$this->monthly_en',
                    'monthly_np' => '$this->monthly_np',
                    'weekly_en' => '$this->weekly_en',
                    'weekly_np' => '$this->weekly_np',
                    'yearly_en' => '$this->yearly_en',
                    'yearly_np' => '$this->yearly_np',
                    'r_order' => 7
                ]
            );
        /*end here */
        /* brishchik */
        DB::table('rashis')
            ->updateOrInsert(
                ['name_en' => 'SCORPIO'],
                [
                    'name_en' => 'SCORPIO',
                    'name_np' => 'बृश्चिक',
                    'description_np' => '(तो, ना, नि, नु, ने, नो, या, यि, यु )',
                    'description_en' => 'October 23 - November 21',
                    'image' => '1-scorpio.png',
                    'other' => json_encode([
                        'lucky_number_en' => '1,2,3',
                        'lucky_color_en' => 'Red, Blue',
                        'lucky_number_np' => '1,2,3',
                        'lucky_color_np' => 'Red, Blue',
                    ]),
                    'daily_en' => '$this->daily_en',
                    'daily_np' => '$this->daily_np',
                    'monthly_en' => '$this->monthly_en',
                    'monthly_np' => '$this->monthly_np',
                    'weekly_en' => '$this->weekly_en',
                    'weekly_np' => '$this->weekly_np',
                    'yearly_en' => '$this->yearly_en',
                    'yearly_np' => '$this->yearly_np',
                    'r_order' => 8
                ]
            );
        /*end here */
        /* brishchik */
        DB::table('rashis')
            ->updateOrInsert(
                ['name_en' => 'SAGITTARIUS'],
                [
                    'name_en' => 'SAGITTARIUS',
                    'name_np' => 'धनु',
                    'description_np' => '(ये, यो, भ, भि, भु, ध, फा, ढ, भे)',
                    'description_en' => 'November 22 - December 21',
                    'image' => '8-sagittarius.png',
                    'other' => json_encode([
                        'lucky_number_en' => '1,2,3',
                        'lucky_color_en' => 'Red, Blue',
                        'lucky_number_np' => '1,2,3',
                        'lucky_color_np' => 'Red, Blue',
                    ]),
                    'daily_en' => '$this->daily_en',
                    'daily_np' => '$this->daily_np',
                    'monthly_en' => '$this->monthly_en',
                    'monthly_np' => '$this->monthly_np',
                    'weekly_en' => '$this->weekly_en',
                    'weekly_np' => '$this->weekly_np',
                    'yearly_en' => '$this->yearly_en',
                    'yearly_np' => '$this->yearly_np',
                    'r_order' => 9
                ]
            );
        /*end here */
        /*makar */
        DB::table('rashis')
            ->updateOrInsert(
                ['name_en' => 'CAPRICORN'],
                [
                    'name_en' => 'CAPRICORN',
                    'name_np' => 'मकर',
                    'description_np' => '(भो, ज, जि, खि, खु, खे, खो, गा, गि)',
                    'description_en' => 'December 22 - January 19',
                    'image' => '3-capricorn.png',
                    'other' => json_encode([
                        'lucky_number_en' => '4,8,2',
                        'lucky_color_en' => 'Red, Blue',
                        'lucky_number_np' => '4,8,2',
                        'lucky_color_np' => 'Red, Blue',
                    ]),
                    'daily_en' => '$this->daily_en',
                    'daily_np' => '$this->daily_np',
                    'monthly_en' => '$this->monthly_en',
                    'monthly_np' => '$this->monthly_np',
                    'weekly_en' => '$this->weekly_en',
                    'weekly_np' => '$this->weekly_np',
                    'yearly_en' => '$this->yearly_en',
                    'yearly_np' => '$this->yearly_np',
                    'r_order' => 10
                ]
            );
        /*end here */
         /*kunbha */
         DB::table('rashis')
         ->updateOrInsert(
             ['name_en' => 'AQUARIUS'],
             [
                 'name_en' => 'AQUARIUS',
                 'name_np' => 'कुम्भ',
                 'description_np' => '( गु, गे, गो, सा, सि, सु, से, सो, द)',
                 'description_en' => 'January 20 - February 18',
                 'image' => '11-aquarius.png',
                 'other' => json_encode([
                     'lucky_number_en' => '4,6,5',
                     'lucky_color_en' => 'Red, Blue',
                     'lucky_number_np' => '4,6,5',
                     'lucky_color_np' => 'Red, Blue',
                 ]),
                 'daily_en' => '$this->daily_en',
                 'daily_np' => '$this->daily_np',
                 'monthly_en' => '$this->monthly_en',
                 'monthly_np' => '$this->monthly_np',
                 'weekly_en' => '$this->weekly_en',
                 'weekly_np' => '$this->weekly_np',
                 'yearly_en' => '$this->yearly_en',
                 'yearly_np' => '$this->yearly_np',
                 'r_order' => 11
             ]
         );
     /*end here */
        /* min */
        DB::table('rashis')
            ->updateOrInsert(
                ['name_en' => 'PISCES'],
                [
                    'name_en' => 'PISCES',
                    'name_np' => 'मीन',
                    'description_np' => '( दि, दु, थ, झ, ञ, दे, दो, चा, चि )',
                    'description_en' => 'February 19 - March 20',
                    'image' => '9-pisces.png',
                    'other' => json_encode([
                        'lucky_number_en' => '1,2,3',
                        'lucky_color_en' => 'Red, Blue',
                        'lucky_number_np' => '1,2,3',
                        'lucky_color_np' => 'Red, Blue',
                    ]),
                    'daily_en' => '$this->daily_en',
                    'daily_np' => '$this->daily_np',
                    'monthly_en' => '$this->monthly_en',
                    'monthly_np' => '$this->monthly_np',
                    'weekly_en' => '$this->weekly_en',
                    'weekly_np' => '$this->weekly_np',
                    'yearly_en' => '$this->yearly_en',
                    'yearly_np' => '$this->yearly_np',
                    'r_order' => 12
                ]
            );
        /*end here */
    }
}
