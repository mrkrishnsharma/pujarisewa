<?php

use Illuminate\Database\Seeder;

class SpecialitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('specialities')
            ->updateOrInsert(
                ['name_en' => 'Bratabandha'],
                [
                    'name_en' => 'Bratabandha',
                    'name_np' => 'Bratabandha',
                    'status'=>1
                ]
            );
        DB::table('specialities')
            ->updateOrInsert(
                ['name_en' => 'Bibah'],
                [
                    'name_en' => 'Bibah',
                    'name_np' => 'Bibah',
                    'status'=>1
                ]
            );
        DB::table('specialities')
            ->updateOrInsert(
                ['name_en' => 'Nuaran'],
                [
                    'name_en' => 'Nuaran',
                    'name_np' => 'Nuaran',
                    'status'=>1
                ]
            );
        DB::table('specialities')
            ->updateOrInsert(
                ['name_en' => 'Saptah'],
                [
                    'name_en' => 'Saptah',
                    'name_np' => 'Saptah',
                    'status'=>1
                ]
            );
        DB::table('specialities')
            ->updateOrInsert(
                ['name_en' => 'Grahasanti'],
                [
                    'name_en' => 'Grahasanti',
                    'name_np' => 'Grahasanti',
                    'status'=>1
                ]
            );
        DB::table('specialities')
            ->updateOrInsert(
                ['name_en' => 'Chaturthi'],
                [
                    'name_en' => 'Chaturthi',
                    'name_np' => 'Chaturthi',
                    'status'=>1
                ]
            );
    }
}
