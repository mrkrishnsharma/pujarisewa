<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return 'Welcome to Pujari Sewa API.';
});

Route::get('/migrate/{key}',  function($key = null)
{
    if($key == "Yt67b8h"){
        try {
            echo '<br>init migration seed';
            Artisan::call('migrate:refresh',[
                '--seed' => true,
            ]);
            echo "<br>";
            echo "loading------";
            echo "<br>";
            echo 'done migrate seed';
        } catch (Exception $e) {
            Response::make($e->getMessage(), 500);
        }
    }else{
        App::abort(404);
    }
});