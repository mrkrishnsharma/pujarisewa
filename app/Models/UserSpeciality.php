<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSpeciality extends Model
{
    protected $table = 'user_specialities';
}
