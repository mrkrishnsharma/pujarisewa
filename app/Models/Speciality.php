<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Speciality extends Model
{
    protected $table = 'specialities';

    public function users()
    {
        return $this->belongsToMany('App\User','user_specialities');
    }
}
