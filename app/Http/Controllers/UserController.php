<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    /**
     * @SWG\Get(
     *      path="/users",
     *      summary="Get list of users",
     *      description="Returns list of users",
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       )
     *     )
     *
     * Returns list of projects
     */
    public function index(Request $request)
    {
        $users = User::with('userdistrict')->where('type','pujari');

        if($request->has('district_id')){
            $users->where('district_id',$request->get('district_id'));
        }

        $users = $users->paginate();


        return UserResource::collection($users)
        ->additional(['status_code' => 200]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @SWG\Post(
     *     path="/users",
     *     description="create new user",
     *     @SWG\Parameter(
     *          name="body",
     *          in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/StoreRequest")
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Created",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Unprocessable Entity"
     *     ),
     * )
     */
    public function store(StoreRequest $request)
    {
        $user = User::create($request->validated());

        return (new UserResource($user));
    }

    /**
     * @SWG\Get(
     *      path="/users/{id}",
     *      summary="Get user information",
     *      description="Returns users data",
     *      @SWG\Parameter(
     *          name="id",
     *          description="user id",
     *          required=true,
     *          type="integer",
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=404, description="Resource Not Found")
     * )
     *
     */
    public function show(User $user)
    {
        return new UserResource($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * update a resource in storage.
     *
     * @SWG\Patch(
     *     path="/users/{id}",
     *     description="update new user",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          type="integer",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *          name="body",
     *          in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/UpdateRequest")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="updated",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Unprocessable Entity"
     *     ),
     * )
     */
    public function update(UpdateRequest $request, User $user)
    {
        $user->update($request->validated());
        // test
        return new UserResource($user);
    }

    /**
     * @SWG\Delete(
     *      path="/users/{id}",
     *      summary="delete user",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          type="integer",
     *         required=true,
     *     ),
     *      @SWG\Response(
     *          response=204,
     *          description="no content deleted."
     *       )
     *     )
     *
     * Returns list of projects
     */
    public function destroy(User $user)
    {
        //$user->delete();

        return response()->json(null, 204);
    }
}
