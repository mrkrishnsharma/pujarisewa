<?php

namespace App\Http\Controllers;

use App\Http\Resources\DistrictResource;
use App\Models\District;
use Illuminate\Http\Request;

class DistrictController extends Controller
{
    /**
     * @SWG\Get(
     *      path="/districts",
     *      summary="Get list of districts",
     *      description="Returns list of districts",
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       )
     *     )
     *
     * Returns list of projects
     */
    public function index(Request $request)
    {
        $districts = District::where('status',1);

        $districts = $districts->paginate();

        return DistrictResource::collection($districts)
            ->additional(['status_code' => 200]);
    }
}
