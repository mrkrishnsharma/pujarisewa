<?php

namespace App\Http\Controllers;

use App\Http\Resources\RashiResource;
use App\Models\Rashi;
use Illuminate\Http\Request;

class RashiController extends Controller
{

    /**
     * @SWG\Get(
     *      path="/users",
     *      summary="Get list of users",
     *      description="Returns list of users",
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       )
     *     )
     *
     * Returns list of projects
     */
    public function index(Request $request)
    {
        $rashis = Rashi::orderBy('r_order','asc')->get();
        return RashiResource::collection($rashis)
        ->additional(['status_code' => 200,
            'daily_date'=>'Jan 21',
            'monthly_date'=>'January',
            'weekly_date'=>'Jan 19 - Jan 26',
            'yearly_date'=>'2020',
        ]);
    }


    public function store(Request $request)
    {
        if($request->get('passkey') == 'fasd#45541212sdfsdf'){
            $rashis = $request->get('rashis');
            foreach($rashis as $rashi){
                // return $rashi['id'];
                $singlerashi = Rashi::find($rashi['id']);
                $singlerashi->daily_en = $rashi['daily_en'];
                $singlerashi->daily_np = $rashi['daily_np'];
                $singlerashi->monthly_en = $rashi['monthly_en'];
                $singlerashi->monthly_np = $rashi['monthly_np'];
                $singlerashi->weekly_en = $rashi['weekly_en'];
                $singlerashi->weekly_np = $rashi['weekly_np'];
                $singlerashi->yearly_en = $rashi['yearly_en'];
                $singlerashi->yearly_np = $rashi['yearly_np'];
                $singlerashi->save();
            }
            return response(200);
        }
        abort(500);
    }
}
