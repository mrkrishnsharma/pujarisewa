<?php

namespace App\Http\Resources;

use App\Models\Speciality;
use App\Models\UserSpeciality;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Http\Resources\RelatedUserResource;

class UserResource extends JsonResource
{
    /**
     * Class UserResource
     * @package App\Http\Resources
     */

    public function toArray($request)
    {

        return [
            'id'=>$this->id,
            'first_name'=>$this->first_name,
            'last_name'=>$this->last_name,
            'email'=>$this->email,
            'mobile'=>$this->mobile,
            'dob'=>$this->dob,
            'address'=>$this->address,
            'district_id'=>$this->district_id,
            'district'=>$this->userdistrict,
            'cost_day'=>$this->cost_day,
            'experience_year'=>$this->experience_year,
            'latitude'=>$this->latitude,
            'longitude'=>$this->longitude,
            'description'=>$this->description,
            'image'=> asset('images/placeholder.jpg'),
            'specialities'=>$this->specialities,
            'related_users' => RelatedUserResource::collection(User::where('type','pujari')->inRandomOrder()->limit(4)->get())
        ];
    }
}
