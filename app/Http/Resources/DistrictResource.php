<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DistrictResource extends JsonResource
{
    /**
     * Class UserResource
     * @package App\Http\Resources
     */

    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name_en'=>$this->name_en,
            'name_np'=>$this->name_np,
        ];
    }
}
