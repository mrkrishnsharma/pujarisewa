<?php

namespace App\Http\Resources;

use App\Models\Speciality;
use App\Models\UserSpeciality;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;


class RelatedUserResource extends JsonResource
{
    /**
     * Class UserResource
     * @package App\Http\Resources
     */

    public function toArray($request)
    {

        return [
            'id'=>$this->id,
            'first_name'=>$this->first_name,
            'last_name'=>$this->last_name,
            'email'=>$this->email,
            'mobile'=>$this->mobile,
            'dob'=>$this->dob,
            'address'=>$this->address,
            'cost_day'=>$this->cost_day,
            'experience_year'=>$this->experience_year,
            'latitude'=>$this->latitude,
            'longitude'=>$this->longitude,
            'description'=>$this->description,
            'image'=> asset('images/placeholder.jpg'),
            'specialities'=>$this->specialities,
            'district'=>$this->district->name
        ];
    }
}
