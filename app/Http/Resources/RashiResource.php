<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RashiResource extends JsonResource
{
    /**
     * Class UserResource
     * @package App\Http\Resources
     */

    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name_en'=>$this->name_en,
            'name_np'=>$this->name_np,
            'description_en'=>$this->description_en,
            'description_np'=>$this->description_np,
            'image'=>asset('images/rashis').'/'.$this->image,
            'other'=>json_decode($this->other),
            'daily_en'=>$this->daily_en,
            'daily_np'=>$this->daily_np,
            'monthly_en'=>$this->monthly_en,
            'monthly_np'=>$this->monthly_np,
            'weekly_en'=>$this->weekly_en,
            'weekly_np'=>$this->weekly_np,
            'yearly_en'=>$this->yearly_en,
            'yearly_np'=>$this->yearly_np,
            'daily_date'=>'Jan 21',
            'monthly_date'=>'January',
            'weekly_date'=>'Jan 19 - Jan 26',
            'yearly_date'=>'2020',
        ];
    }
}
